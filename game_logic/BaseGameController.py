#!/usr/bin/python3
# -*- coding: utf-8 -*-


class BaseGameController():
	"""docstring for BaseGameController"""
	ELEM_DOT_SANTA = 'S'
	ELEM_DOT_ENEMY = 'P'
	ELEM_DOT_EMPTY = 'E'
	ELEM_DOT_WALL  = 'X'

	STEP_LABEL_ENEMY = 'Ход школьников'
	STEP_LABEL_SANTA = 'Ход санты'
	STEP_LABEL_SANTA_CHOOSE_POSITION = 'Санта выбирает позицию'
	STEP_LABEL_GAME_END = 'Игра окончена'
	STEP_LABEL_SANTA_WIN = 'Санта победил'
	STEP_LABEL_ENEMY_WIN = 'Школьники победили'

	FIELD_LEN = 7

	BGC_DEBUG = False

	def __init__(self):
		super().__init__()
		self.game_field = self.base_game_field()
		self.step = 0;
		self.santa_position = None
		if self.BGC_DEBUG: print('bgc_init')

	def base_game_field(self):
		if self.BGC_DEBUG: print('bgc_base_game_field')
		return [
			[self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL,  self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL],
			[self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL,  self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL],
			[self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY],
			[self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_EMPTY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY],
			[self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY],
			[self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL,  self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL],
			[self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL,  self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_ENEMY, self.ELEM_DOT_WALL,  self.ELEM_DOT_WALL],
		]

	def get_game_field(self): 
		if self.BGC_DEBUG: print('bgc_get_game_field')
		if self.BGC_DEBUG: self.dd_gamefield()
		return self.game_field

	def next_step(self):
		if self.BGC_DEBUG: print('bgc_next_step')
		print('next step')
		self.step += 1
		if self.is_santa_step(): self.action_santa_step()
		else: self.action_enemy_step()

	def get_step(self):
		if self.BGC_DEBUG: print('bgc_get_step') 
		return self.step

	def is_santa_step(self):
		if self.BGC_DEBUG: print('bgc_is_santa_step')
		print(' is santa step')
		return self.get_step() % 2 != 0

	def get_step_label(self):
		if self.BGC_DEBUG: print('bgc_get_step_label') 
		return self.STEP_LABEL_ENEMY if self.get_step() % 2 == 0 else self.STEP_LABEL_SANTA

	def action_santa_step(self):
		pass

	def action_santa_set_position_step(self):
		pass

	def action_enemy_step(self):
		pass

	def action_gameend(self, winner):
		pass

	def action_step(self, new_pos, old_pos=None):
		if self.BGC_DEBUG: print('bgc_action_step')
		print('bgc_action_step: {} {}'.format(new_pos, old_pos))
		self.is_endgame()

		if self.is_santa_step() or old_pos is None:
			print('Santa step True')
			return self.validate_santa_step(new_pos, self.santa_position)
		else:
			print("Santa step false")
			return self.validate_enemy_step(new_pos, old_pos)
		return False

	def validate_enemy_step(self, new_pos, old_pos):
		if self.BGC_DEBUG: print('bgc_validate_enemy_step')
		print('Enemy_validate')
		try:
			old_pos_dot = self.game_field[old_pos[0]][old_pos[1]]
			new_pos_dot = self.game_field[new_pos[0]][new_pos[1]]
		except Exception as e:
			print('False 1')
			return False

		if old_pos_dot == self.ELEM_DOT_ENEMY and new_pos_dot == self.ELEM_DOT_EMPTY:
			if ((old_pos[0] - 1) <= new_pos[0] <= (old_pos[0] + 1) and old_pos[1] == new_pos[1]) or (
				(old_pos[1] - 1) <= new_pos[1] <= (old_pos[1] + 1) and old_pos[0] == new_pos[0]):
				self.enemy_step(new_pos, old_pos)
				print('True 1')
				return True
		return False
		print("False 2")

	def enemy_step(self, new_pos, old_pos):
		if self.BGC_DEBUG: print('bgc_enemy_step')
		print('Enemy step')
		self.game_field[old_pos[0]][old_pos[1]] = self.ELEM_DOT_EMPTY
		self.game_field[new_pos[0]][new_pos[1]] = self.ELEM_DOT_ENEMY
		self.next_step()

	def validate_santa_step(self, new_pos, old_pos):
		if self.BGC_DEBUG: print('bgc_validate_santa_step')
		print('bgc_validate_santa_step')
		print('Santa Old Pos: [{}, {}]'.format(old_pos[0], old_pos[1]))
		print('Santa New Pos: [{}, {}]'.format(new_pos[0], new_pos[1]))
		try:
			old_pos_dot = self.game_field[old_pos[0]][old_pos[1]]
			new_pos_dot = self.game_field[new_pos[0]][new_pos[1]]
		except Exception as e:
			return False

		if old_pos_dot == self.ELEM_DOT_SANTA and new_pos_dot == self.ELEM_DOT_EMPTY:
			if abs(old_pos[0] - new_pos[0]) > 1 or abs(old_pos[1] - new_pos[1]) > 1:
				if (new_pos[0] == old_pos[0] - 2 or new_pos[0] == old_pos[0] + 2) and (new_pos[1] == old_pos[1] - 2 or new_pos[1] == old_pos[1] + 2) and self.has_enemy_between(new_pos, old_pos):
					self.santa_fight_and_step(new_pos, old_pos)
					return True
			else:
				self.santa_step(new_pos, old_pos)
				return True
		return False

	def validate_santa_start_position(self, pos):
		if self.BGC_DEBUG: print('bgc_validate_santa_start_position')
		if self.is_empty_pos(pos):
			self.game_field[pos[0]][pos[1]] = self.ELEM_DOT_SANTA
			self.santa_position = pos
			self.action_enemy_step()
			return True
		else: return False

	def get_between_position(self, new_pos, old_pos):
		if self.BGC_DEBUG: print('bgc_get_between_position')
		return [
			int((old_pos[0] + new_pos[0]) / 2),
			int((old_pos[1] + new_pos[1]) / 2),
		]

	def has_enemy_between(self, new_pos, old_pos):
		if self.BGC_DEBUG: print('bgc_has_enemy_between')
		pos = self.get_between_position(new_pos, old_pos)
		return self.game_field[pos[0]][pos[1]] == self.ELEM_DOT_ENEMY
		
	def santa_fight_and_step(self, new_pos, old_pos):
		if self.BGC_DEBUG: print('bgc_santa_fight_and_step')
		pos = self.get_between_position(new_pos, old_pos)
		self.game_field[pos[0]][pos[1]] = self.ELEM_DOT_EMPTY
		self.santa_step(new_pos, old_pos)

	def santa_step(self, new_pos, old_pos):
		if self.BGC_DEBUG: print('bgc_santa_step')
		self.game_field[old_pos[0]][old_pos[1]] = self.ELEM_DOT_EMPTY
		self.game_field[new_pos[0]][new_pos[1]] = self.ELEM_DOT_SANTA
		self.santa_position = new_pos
		self.next_step()

	def is_empty_pos(self, pos):
		if self.BGC_DEBUG: print('bgc_is_empty_pos')
		try:
			return self.game_field[pos[0]][pos[1]] == self.ELEM_DOT_EMPTY
		except Exception as e:
			return False

	def is_enemy_pos(self, pos):
		if self.BGC_DEBUG: print('bgc_is_ememy_pos')
		try:
			return self.game_field[pos[0]][pos[1]] == self.ELEM_DOT_ENEMY
		except Exception as e:
			return False

	def is_santa_pos(self, pos):
		if self.BGC_DEBUG: print('bgc_is_santa_pos')
		try:
			return self.game_field[pos[0]][pos[1]] == self.ELEM_DOT_SANTA
		except Exception as e:
			return False

	def get_valid_positions_list(self, pos):
		if self.BGC_DEBUG: print('bgc_get_valid_positions_list')
		try:
			actor_dot = self.game_field[pos[0]][pos[1]]
		except Exception as e:
			return None

		valid_positions = []
		if actor_dot == self.ELEM_DOT_ENEMY:
			for i in range(pos[0] - 1, pos[0] + 2, 2):
				if self.is_empty_pos([i, pos[1]]): valid_positions.append([i, pos[1]])
			for i in range(pos[1] - 1, pos[1] + 2, 2):
				if self.is_empty_pos([pos[0], i]): valid_positions.append([pos[0], i])
		elif actor_dot == self.ELEM_DOT_SANTA:
			for i in range(pos[0] - 1, pos[0] + 2):
					for j in range(pos[1] - 1, pos[1] + 2):
						if self.is_empty_pos([i, j]): valid_positions.append([i, j])
			
			if self. is_empty_pos([pos[0] - 2, pos[1] + 2]) and self.has_enemy_between(pos, [pos[0] - 2, pos[1] + 2]): valid_positions.append([pos[0] - 2, pos[1] + 2])
			if self. is_empty_pos([pos[0] + 2, pos[1] + 2]) and self.has_enemy_between(pos, [pos[0] + 2, pos[1] + 2]): valid_positions.append([pos[0] + 2, pos[1] + 2])
			if self. is_empty_pos([pos[0] + 2, pos[1] - 2]) and self.has_enemy_between(pos, [pos[0] + 2, pos[1] - 2]): valid_positions.append([pos[0] + 2, pos[1] - 2])
			if self. is_empty_pos([pos[0] - 2, pos[1] - 2]) and self.has_enemy_between(pos, [pos[0] - 2, pos[1] - 2]): valid_positions.append([pos[0] - 2, pos[1] - 2])

		return None if not valid_positions else valid_positions

	def is_endgame(self):
		print('validate endgame')
		if self.get_valid_positions_list(self.santa_position) is None:
			print('santa lose')
			self.action_gameend(self.STEP_LABEL_ENEMY_WIN)
		enemy_count = 0
		for i in self.game_field:
			for x in i:
				if x == self.ELEM_DOT_ENEMY: enemy_count += 1
		if enemy_count <= 6: 
			print('enemy lose')
			self.action_gameend(self.STEP_LABEL_SANTA_WIN)

	def init_new_game(self):
		if self.BGC_DEBUG: print('bgc_init_new_game')
		self.game_field = self.base_game_field()
		self.step = 0;
		self.santa_position = None
		self.action_santa_set_position_step()

	def dd_gamefield(self):
		print('bgc_gf_dd:')
		for gf_part in self.game_field:
			for val in gf_part:
				print(val, end=' ')
			print()
