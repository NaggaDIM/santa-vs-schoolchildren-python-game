#!/usr/bin/python3
# -*- coding: utf-8 -*-
from game_logic.BaseGameController import BaseGameController

class GUIGameController(BaseGameController):
	"""docstring for GUIGameController"""
	def __init__(self, ui):
		super().__init__()
		self.ui = ui
		self.choosen_position = None
		self.game_end = False
		if self.GGC_DEBUG: print('ggc_init')

	def init_new_gui_game(self):
		if self.GGC_DEBUG: print('ggc_init_new_game')
		self.choosen_position = None
		self.game_end = False
		self.init_new_game()

	GGC_DEBUG = False

	BACK_LAY_WALL_DOT  = 'X'
	BACK_LAY_FIELD_DOT = 'F'
	BACK_LAY_SIZE = 8

	def background_layer(self):
		if self.GGC_DEBUG: print('ggc_background_layer')
		return  [
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT],
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT],
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT],
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_WALL_DOT],
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_WALL_DOT],
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT],
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_FIELD_DOT, self.BACK_LAY_FIELD_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT],
			[self.BACK_LAY_WALL_DOT, self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT,  self.BACK_LAY_WALL_DOT]
		]

	POS_LAY_EMPTY_DOT = ''
	POS_LAY_SIZE = 7

	def positions_layer(self):
		if self.GGC_DEBUG: print('ggc_positions_layer')
		return [
			['',   '',   '31', '32', '33', '',   ''  ],
			['',   '',   '28', '29', '30', '',   ''  ],
			['21', '22', '23', '24', '25', '26', '27'],
			['14', '15', '16', '17', '18', '19', '20'],
			['7',  '8',  '9',  '10', '11', '12', '13'],
			['',   '',   '4',  '5',  '6',  '',   ''  ],
			['',   '',   '1',  '2',  '3',  '',   ''  ]
		]

	def get_clicked_position(self, x, y):
		if self.GGC_DEBUG: print('ggc_get_clicked_position')
		i = self.get_axis_position(y)
		j = self.get_axis_position(x)
		if i is None or j is None: return None
		print('Clicked: [{}, {}]'.format(i, j))
		return [i, j]

	def get_axis_position(self, axis_value):
		if self.GGC_DEBUG: print('ggc_get_axis_position')
		if 60 <  axis_value < 100: return 0
		if 140 < axis_value < 180: return 1
		if 220 < axis_value < 260: return 2
		if 300 < axis_value < 340: return 3
		if 380 < axis_value < 420: return 4
		if 460 < axis_value < 500: return 5
		if 540 < axis_value < 580: return 6
		return None
		
	def default_mouse_event(self, event): 
		if self.GGC_DEBUG: print('ggc_default_mouse_event')
		return self.get_clicked_position(event.pos().x(), event.pos().y())

	def action_santa_set_position_step(self):
		if self.game_end: return
		if self.GGC_DEBUG: print('ggc_action_santa_set_position_step')
		self.ui.updateLabel(self.STEP_LABEL_SANTA_CHOOSE_POSITION)
		self.ui.mousePressEvent = self.santa_choose_position_event
		self.ui.update()

	def santa_choose_position_event(self, event):
		if self.game_end: return
		if self.GGC_DEBUG: print('ggc_santa_choose_position_event')
		pos = self.default_mouse_event(event)
		if pos is not None:
			self.validate_santa_start_position(pos)

	def action_enemy_step(self):
		if self.game_end: return
		if not self.is_santa_step():
			if self.GGC_DEBUG: print('ggc_action_enemy_step')
			self.ui.updateLabel(self.get_step_label())
			if self.choosen_position is None:
				self.ui.mousePressEvent = self.choose_enemy_event
			else:
				self.ui.mousePressEvent = self.enemy_step_event
			self.ui.update()
		else: self.action_santa_step()

	def choose_enemy_event(self, event):
		if self.GGC_DEBUG: print('ggc_choose_enemy_event')
		if self.game_end: return
		print('ggc_choose_enemy_event')
		pos = self.default_mouse_event(event)
		if pos is not None and self.is_enemy_pos(pos):
			self.choosen_position = pos
		self.action_enemy_step()

	def enemy_step_event(self, event):
		if self.GGC_DEBUG: print('ggc_enemy_step_event')
		if self.game_end: return
		print('ggc_enemy_step_event')
		pos = self.default_mouse_event(event)
		if pos is not None:
			ch_pos = self.choosen_position
			self.choosen_position = None
			self.action_step(pos, ch_pos)
		self.action_enemy_step()

	def action_santa_step(self):
		if self.game_end: return
		if self.is_santa_step():
			if self.GGC_DEBUG: print('ggc_action_santa_step')
			self.ui.updateLabel(self.get_step_label())
			self.ui.mousePressEvent = self.santa_step_event
			self.ui.update()
		else: self.action_enemy_step()

	def santa_step_event(self, event):
		if self.game_end: return
		if self.GGC_DEBUG: print('ggc_santa_step_event')
		print('ggc_santa_step_event')
		pos = self.default_mouse_event(event)
		if pos is not None:
			self.action_step(pos)
		self.action_santa_step()

	def action_gameend(self, winner):
		print('ggc_action_gameend')
		self.ui.updateLabel(winner)
		self.ui.mousePressEvent = None
		self.game_end = True
		self.ui.update()