#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QWidget, QPushButton, QLabel, QMessageBox
from PyQt5.QtCore import pyqtSlot
from game_logic.GUIGameController import GUIGameController
from PyQt5.QtGui import QPainter, QBrush, QPen, QImage
from PyQt5.QtCore import Qt
import res.resources as resources


class MainUI(QWidget):

	MUI_DEBUG = False

	WINDOW_HEIGHT = 640
	GAME_FIELD_WIDTH = 640
	MENU_X_POSITION = GAME_FIELD_WIDTH
	MENU_FIELD_WIDTH = 200
	PROG_WINDOW_WIDTH = GAME_FIELD_WIDTH + MENU_FIELD_WIDTH
	BTN_X_POSITION = MENU_X_POSITION + 5
	BTN_WIDTH = MENU_FIELD_WIDTH - 10
	BTN_HEIGHT = 30

	BACKGROUND_FIELD_SECTION_SIZE = 80
	BACKGROUND_POSITION_SIZE = 30

	def __init__(self):
		super().__init__()
		self.controller = GUIGameController(self)
		self.step_label = QLabel(self)
		self.initUI()
		self.controller.action_santa_set_position_step()
		if self.MUI_DEBUG: print('mui_init')

	def initUI(self):
		if self.MUI_DEBUG: print('mui_init_ui')
		self.setGeometry(200, 200, self.PROG_WINDOW_WIDTH, self.WINDOW_HEIGHT)
		self.setFixedSize(self.PROG_WINDOW_WIDTH, self.WINDOW_HEIGHT)
		self.setWindowTitle('Santa vs Schoolchildren PyGame')

		new_game_btn = QPushButton('Новая Игра', self)
		new_game_btn.setGeometry(
			self.BTN_X_POSITION,
			5,
			self.BTN_WIDTH,
			self.BTN_HEIGHT
		)
		new_game_btn.clicked.connect(self.new_game)

		rules_btn = QPushButton('Правила', self)
		rules_btn.setGeometry(
			self.BTN_X_POSITION,
			self.BTN_HEIGHT + 10,
			self.BTN_WIDTH,
			self.BTN_HEIGHT
		)
		rules_btn.clicked.connect(self.view_rules)

		about_btn = QPushButton('О разработчиках', self)
		about_btn.setGeometry(
			self.BTN_X_POSITION,
			self.WINDOW_HEIGHT - self.BTN_HEIGHT - 5,
			self.BTN_WIDTH,
			self.BTN_HEIGHT
		)
		about_btn.clicked.connect(self.view_about)

		self.step_label.setGeometry(
			self.BTN_X_POSITION,
			(self.BTN_HEIGHT * 2) + (5 * 3),
			self.BTN_WIDTH,
			self.BTN_HEIGHT
		)
		self.show()

	def updateLabel(self, label):
		if self.MUI_DEBUG: print('mui_updateLabel')
		self.step_label.setText(label)

	@pyqtSlot()
	def new_game(self):
		if self.MUI_DEBUG: print('mui_new_game')
		self.controller.init_new_gui_game()

	@pyqtSlot()
	def view_rules(self):
		QMessageBox().information(self, "Правила", "1. Цель санты - наказать всех злых школьников!\n2. Цель школьников - Запереть Санту и отобрать подарки\n3. Игра начинаеться с того что Санта выбирает свою начальную позицию\n4. Однако первый ход пренадлежит школьникам\n5. Школьники могут совершать ход на 1 клетку только по прямой линии (Вверх, Вниз, Влево, Вправо)\n6. Санта может передвигаться на 1 клетку в любом направлении или на 2 по диагонали для того чтоб наказать школьника\n7. Санта может поймать и наказать школьника, для этого ему нужно перепрыгнуть через него (Как в шашках), однако, за 1 ход Санта может наказать только 1 школьника (One Step - One Kill)\n8. Если на поле остаётся 6 школьников, то Санта побеждает\n9. Если Школьники перекрывают все возможные ходы Санты, то Школьники побеждают")
		
	@pyqtSlot()
	def view_about(self):
		QMessageBox().information(self,
				"О разработчиках", 
				"Команда разработчиков игры состоит из 3-х студентов Нижегородского Губернского Колледжа группы 31П:\n\t{}\n\t{}\n\t{}\n\nСайт разработчика (1): {}\n".format(
					'1. Смертин Дмитрий Анатольевич (NaggaDIM)',
					'2. Масленников Дмитрий Алексеевич (mrMDA28)',
					'3. Малюков Семён Валерьевич (simeon_pravednik)',
					'http://naggadim.ru'
			))

	def paintEvent(self, event):
		if self.MUI_DEBUG: print('mui_paintEvent')
		painter = QPainter(self)
		self.paintBackgroundLayer(painter)
		self.paintPositionsLayer(painter)
		self.paintActors(painter)

	def paintBackgroundLayer(self, painter):
		game_field_img = QImage(':/game_field/field.png')
		painter.drawImage(0,0, game_field_img)

	def paintPositionsLayer(self, painter):
		if self.MUI_DEBUG: print('mui_paintPositionsLayer')
		pos_lay = self.controller.positions_layer()
		actives = self.controller.get_valid_positions_list(self.controller.santa_position if self.controller.is_santa_step() else self.controller.choosen_position)
		for x in range(self.controller.POS_LAY_SIZE):
			for y in range(self.controller.POS_LAY_SIZE):
				if pos_lay[y][x] != self.controller.POS_LAY_EMPTY_DOT: self.paintActivePosition(painter, x, y, pos_lay[y][x]) if self.is_active(actives, [y, x]) else self.paintPosition(painter, x, y, pos_lay[y][x])

	def is_active(self, actives, pos):
		try:
			for a_pos in actives:
				if a_pos[0] == pos[0] and a_pos[1] == pos[1]: return True
			return False
		except Exception as e:
			return False
		

	def paintActivePosition(self, painter, x, y, pos_dot):
		if self.MUI_DEBUG: print('mui_paintPosition')
		pos_img = QImage(':/positions_active/{}.png'.format(pos_dot))
		painter.drawImage(x * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70, y * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70, pos_img)


	def paintPosition(self, painter, x, y, pos_dot):
		if self.MUI_DEBUG: print('mui_paintPosition')
		pos_img = QImage(':/positions/{}.png'.format(pos_dot))
		painter.drawImage(x * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70, y * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70, pos_img)

	def paintActors(self, painter):
		if self.MUI_DEBUG: print('mui_paintActors')
		for x in range(7):
			for y in range(7):
				if self.controller.is_santa_pos([y, x]): self.paintSanta(painter, x, y)
				if self.controller.is_enemy_pos([y, x]): self.paintEnemy(painter, x, y)

	def paintSanta(self, painter, x, y):
		if self.MUI_DEBUG: print('mui_paintSanta')
		santa_img = QImage(':/actors/santa.png')
		painter.drawImage(
			x * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70,
			y * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70,
			santa_img,
		)

	def paintEnemy(self, painter, x, y):
		if self.MUI_DEBUG: print('mui_paintEnemy')
		enemy_img = QImage(':/actors/{}.png'.format('enemy_active' if self.is_selected_enemy(x, y) else 'enemy'))
		painter.drawImage(
			x * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70,
			y * self.BACKGROUND_FIELD_SECTION_SIZE - (self.BACKGROUND_POSITION_SIZE / 3) + 70,
			enemy_img,
		)

	def is_selected_enemy(self, x, y):
		if self.MUI_DEBUG: print('mui_is_selected_enemy')
		if self.controller.choosen_position is not None:
			if self.controller.choosen_position[0] == y and x == self.controller.choosen_position[1]: return True
		return False