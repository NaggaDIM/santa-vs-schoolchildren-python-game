#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QApplication
from res.MainUI import MainUI
from game_logic.GUIGameController import GUIGameController


if __name__ == '__main__':
	app = QApplication(sys.argv)
	main_ui = MainUI()
	sys.exit(app.exec_())
